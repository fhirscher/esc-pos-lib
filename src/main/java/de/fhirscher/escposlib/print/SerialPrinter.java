package de.fhirscher.escposlib.print;

import com.fazecast.jSerialComm.SerialPort;

public class SerialPrinter implements Printer {

    private SerialPort port;

    /**
     *
     *
     * @param port
     */
    public SerialPrinter(SerialPort port) {
        this.port = port;
    }

    /**
     * Open the port
     */
    @Override
    public void open() {
        this.port.openPort();
    }

    /**
     * Write data to the port
     *
     * @param command
     */
    @Override
    public void write(byte[] command) {
        this.port.writeBytes(command, command.length);
    }


    /**
     * Close the port
     */
    @Override
    public void close() {
        this.port.closePort();
    }
}
