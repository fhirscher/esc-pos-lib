package de.fhirscher.escposlib;

import de.fhirscher.escposlib.print.NetworkPrinter;
import de.fhirscher.escposlib.print.Printer;

public class App {

   public static void main(String[] args){
      Printer printer = new NetworkPrinter("192.168.0.100", 9100);
      PrinterService printerService = new PrinterService(printer);

      printerService.print("Test text");

      printerService.close();
   }
}
